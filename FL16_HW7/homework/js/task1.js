const minMoney = 1000;
const minYears = 1;
const maxPersent = 100;
const one = 1;
const two = 2;
let totalAmount;
let totalProfit;
let amount = +prompt('Input initial amount of money, >1000', '');
if (amount < minMoney || isNaN(amount)) {
    alert('Invalid input data');
} else {
    let years = +prompt('Input number of years, >1', '');
    let yearsNum = parseInt(years);
    if (years < minYears || isNaN(years)) {
    alert('Invalid input data');   
    } else {
        let persent = +prompt('Input percentage of a year, <100%', '');
        if (persent > maxPersent || isNaN(persent)) {
        alert('Invalid input data');
            }
        totalAmount = amount * Math.pow(one + persent / maxPersent, yearsNum);
        totalProfit = totalAmount - amount;
        alert(`Initial amount: ${amount}
        Number of years: ${yearsNum}
        Percentage of year: ${persent}

        Total profit: ${totalProfit.toFixed(two)}
        Total amount: ${totalAmount.toFixed(two)}`);
    }
}
