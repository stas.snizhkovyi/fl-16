function reverseNumber(num) {
    let startNumber = num;
    if (num < 0) {
        num = -num;
    }
    let str = String(num), res = '';
    for (let i = str.length - 1; i >= 0; i--) {
        res += str[i]
    }
    if (startNumber < 0) {
        return Number(-res);
    }
    return Number(res);
}

function forEach(arr, func) {
    let eachArr = [];
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
    return eachArr;
}

function map(arr, func) {
    const array = [];
    forEach(arr, func);
    return array;
}

function filter(arr, func) {
    let filteredArr = [];
    forEach(arr, (el) => {
        if (func(el)) {
            filteredArr.push(el);
        }
    })
    return filteredArr;
}

function getAdultAppleLovers(data) {
    return data
        .filter(
            people => people.favoriteFruit === 'apple' && people.age >= eighteen
        )
        .map(people => people.name);
}

function getKeys(obj) {
    let newAr = [];
    for (let key in obj) {
        if (key in obj) {
            newAr.push(obj[key]);
        }
    }
    return newAr;
}

function getValues(obj) {
    let newAr = [];
    for (let values in obj) {
        if (values in obj) {
            newAr.push(obj[values]);
        }
    }
    return newAr;
}

function showFormattedDate(dateObj) {
    let dd = (dateObj.getDate() < 10 ? '0' : '') + dateObj.getDate();
    let MM = (dateObj.getMonth() < 10 ? '0' : '') + (dateObj.getMonth() + 1);
    let yyyy = dateObj.getFullYear();
    return 'It is ' + dd + ' of ' + MM + ', ' + yyyy;
}

