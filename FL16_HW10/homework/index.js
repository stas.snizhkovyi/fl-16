// 1 task
function isEqual(a, b) {
  return a === b;
}
// 2 task
function isBigger(a, b) {
  return a > b;
}
// 3 task
function storeNames(arr) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        newArr.push(arr[i]);
    }
    return newArr;
}
// 4 task
function getDifference(a, b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}
// 5 task
const negativeCount = arr => arr.filter(item => item < 0).length;
// 6 task
function letterCount(a, b) {
    return a.split(b).length - 1;
}
// 7 task
function countPoints(points) {
    let three = 3;
  return points.reduce((sum, point) => {
    const [x, y] = point.split(':').map((i) => parseFloat(i));
    if (x > y) {
      sum += three;
    } else if (x < y) {
      sum += 0;
    } else if (x === y) {
      sum += 1;
    }
    return sum;
  }, 0);
}
